''' @file                       encoder.py
    @brief                      A driver that reads from quadrature encoders
    @details                    Driver reads encoder data and writes to encoder 
                                task, and implements signals from encoder task
                                in hardware
    @author                     Ronan Shaffer
    @author                     Nishka Chawla
    @date                       10/19/2021
'''

import pyb

class Encoder:
    ''' @brief                  Class containing methods in encoder driver.
        @details                Class containing update, get_position, 
                                set_position, and get_delta methods to send 
                                information to task_encoder.
    '''
    
    def __init__(self, pinA, pinB, timernum):
        ''' @brief              Constructor for Encoder Class.
            @details            Constructs a timer object in Encoder class.
            @param pinA         Defines the pin for channel 1 of the timer.
            @param pinB         Defines the pin for channel 2 of the timer.
            @param timernum     Defines the number of the timer object.
        '''
        ## @brief   Overflow threshold for timer.counter
        #  @details Defines 16-bit constant value to check delta against 
        #           overflow and correct as needed.
        self.period = 65535
        
        ## @brief   Timer object in pyb class keeps track of time in program
        self.timer = pyb.Timer(4, period = 65535, prescaler = 0)
        self._ch1 = self.timer.channel(1, pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.B6)
        self._ch2 = self.timer.channel(2, pyb.Timer.ENC_AB, pin = pyb.Pin.cpu.B7)
        
        self._prev_count = 0
        self._ENC_POS = 0

        
    def update(self):
        ''' @brief                  Updates encoder position and delta.
            @details                Tracks the current encoder position and 
                                    corrects for overflow.
        '''  
        self._current_count = self.timer.counter()
        ## @brief   Ticks between two most recent encoder positions.
        #  @details Calculates number of ticks between current position and
        #           most recent previous position and corrects for overflow.
        self.delta = self._current_count - self._prev_count
        self._prev_count = self._current_count
        
        if self.delta > self.period/2:
            self.delta -= self.period
        elif self.delta < -self.period/2:
            self.delta += self.period

        self._ENC_POS += self.delta
#        print('Reading encoder count and updating position and delta values')

    def get_position(self):
        ''' @brief                  Returns encoder position
            @details                A function that returns encoder position.
            @return                 The position of the encoder shaft
        '''
        return self._ENC_POS
    
    def set_position(self, position):
        ''' @brief                  Sets encoder position to specified value.
            @details                A function that sets the encoder position 
                                    to a specified value.
            @param  position        The new position of the encoder shaft
        '''
        self._prev_count = self.timer.counter()
        self._ENC_POS = position
        print('Setting position and delta values')
        
    def get_delta(self):
        ''' @brief                  Returns encoder delta
            @details                A function that returns the delta value.
            @return                 The change in position of the encoder shaft 
                                    between the two most recent updates
        '''
        return self.delta
        

#if __name__ == "__main__":
#    
#    while(True):
#        encoder1.update()
#        encoder2.update()
#        print(encoder1.get_position())
#        print(encoder2.get_position())
#        encoder1.set_position()
#        encoder2.set_position()
#    