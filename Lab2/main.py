''' @file                       main.py
    @brief                      Runs task_encoder and task_user methods.
    @details                    Iterates through methods in task classes and
                                instantiates share objects. 
                                This is the link to the FSM diagrams: 
                                https://imgur.com/a/XltHUfP
    @author                     Nishka Chawla
    @author                     Ronan Shaffer
    @date                       10/19/2021
'''


# All import statements should go near the top
import pyb
import task_encoder # imports module "task_example.py" (not the class)
import task_user 
import shares
import encoder
## A Task object from the module "task_example.py"
# from the class "Task_Example"

def main():
    ''' @brief              Main loop running the program.
        @details            Main program iterates through task_user and task_encoder
                            methods to communicate between user and encoder.
    '''
    
#    pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
#    pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
#   pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
#   pinC7 = pyb.Pin(pyb.Pin.cpu.C7)
    
    ## @brief   An encoder object in the task_encoder class
    encoder1 = encoder.Encoder(pyb.Pin.cpu.B6,pyb.Pin.cpu.B7,4)
#   encoder2 = Encoder(pinC6,pinC7,3)
    
    ## @brief   A shares.Share object for position
    #  @details The most recent orientation read by the encoder.
    position = shares.Share(0)
    ## @brief   A shares.Share object for delta
    #  @details The number of ticks between the two most recent positions
    #           recorded by the encoder.
    delta = shares.Share(0)
    ## @brief   A shares.Share object for the zero flag
    #  @details A boolean flag used to instruct the encoder task to set the
    #           most recent encoder position to zero.
    z_flag = shares.Share(0)
    ## @brief   A shares.Share object for the task period
    #  @details The period, in microseconds, between runs of the task.
    period = shares.Share(0)
    
    ### An LED task object
#    encoder1 = task_user.Task_User('encoder1', 10_000, encoderG14)
#    encoder2 = task_user.Task_User('encoder2', 10_000, encoderG14)
    
    #  obj = module_name.Class_Name()
    ## @brief   A task_encoder object.
    #  @details Instantiates object in task_encoder class to run method in main
    task1 = task_encoder.Task_Encoder(period, z_flag, position, delta, encoder1)
    ## @brief   A task_user object.
    #  @details Instantiates object in task_user class to run method in main
    task2 = task_user.Task_User(period, z_flag, position, delta, encoder1)
    _task_list = [task1, task2]
    
    while (True):
        # Attempt to run FSM unless Ctrl+C is hit
        try:
            for task in _task_list:
                task.run()  
            
        #Look for Ctrl+C which triggers a KeyboardInterrupt
        except KeyboardInterrupt:
            break # Breaks out of the FSM while loop
    
    # Action taken once while loop is terminated
    print('Program Terminating')

## Functions should be defined before the main part of the program

# The main program should go at the bottom and run continuously until the user exits


if __name__ == '__main__':
    main()


