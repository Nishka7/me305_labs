''' @file                       task_user.py
    @brief                      Interfaces with program user.
    @details                    Receives user input and communicates with 
                                task_encoder to print encoder information 
                                corresponding to relevant user command.
    @author                     Nishka Chawla
    @author                     Ronan Shaffer
    @date                       10/19/2021
'''

import utime, pyb
from micropython import const
#import shares
#import encoder

## State 0 of the user interface task
_S0_INIT    = const(0)
## State 1 of the user interface task
_S1_WAIT    = const(1)
## State 2 of the user interface task
_S2_COLLECT = const(2)
## State 3 of the user interface task
_S3_PRINT   = const(3)

class Task_User():
    ''' @brief      User interface task for cooperative multitasking.
        @details    Implements a finite state machine to take user input
                    commands and communicate with encoder task via shares.
    '''
    
    def __init__(self, period, z_flag, position, delta, encoder1):
        ''' @brief              Constructs an LED task.
            @details            The LED task is implemented as a finite state
                                machine.
            @param period       The period, in microseconds, between runs of 
                                the task.
            @param z_flag       A boolean flag used to instruct the encoder
                                task to set the most recent encoder position
                                to zero.
            @param position     The most recent orientation read by the encoder.
            @param delta        The number of ticks between the two most recent 
                                positions recorded by the encoder.
            @param my_Q         A shares.Queue object used to store and print 
                                encoder position and delta values.
            @param encoder1     An encoder object in the task_user class.
        '''

        ## @brief   The period (in us) between task iterations.
        #  @details Time period that must elapse since last iteration before
        #           task runs again.
        self.period = 10000
        
        ## @brief   A flag that instructs encoder task to zero current position
        #  @details Binary flag that signals to encoder task to run the
        #           set_position method.
        self.z_flag = z_flag
        
        ## @brief   Stores current position of encoder
        self.position = position
        
        ## @brief   Encoder object with attributes from Encoder class
        self.encoder1 = encoder1
        
        ## @brief   Stores difference between two most recent encoder positions
        self.delta = delta
        
        self._ser = pyb.USB_VCP()
        
        ## The state to run on the next iteration of the finite state machine
        self._state = _S0_INIT
        
        ## The number of runs of the state machine
        self._runs = 0
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self._next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
    def run(self):
        ''' @brief Runs one iteration of the FSM
        '''
        _current_time = utime.ticks_us()
        if (utime.ticks_diff(_current_time, self._next_time) >= 0):
            if self._state == _S0_INIT:
                print('Welcome, press \'z\' to zero the position of encoder 1')
                print('Press \'p\' to print out the position of encoder 1')
                print('Press \'d\' to print out the delta for encoder 1')
                print('Press \'g\' to collect encoder 1 data for 30 seconds')
                print('Press \'s\' to end data collection prematurely.')
                self.transition_to(_S1_WAIT)
                
            elif self._state == _S1_WAIT:
                
                if self._ser.any():
                    ## @brief   Stores user input command as a string
                    char_in = self._ser.read(1).decode()
                    
                    if(char_in == 'z' or char_in == 'Z'):
                        self.z_flag.write(1)
                        
                    elif(char_in == 'p' or char_in == 'P'):
                        print(self.position.read())
                        
                    elif(char_in == 'd' or char_in == 'D'):
                        print(self.delta.read())
                        
                    elif(char_in == 'g' or char_in == 'G'):
                        self._g_time = _current_time
                        self.transition_to(_S2_COLLECT)

                    elif(char_in == 's' or char_in == 'S'):
#                        brt = self.LED_share.read() - 2
#                        if (brt < 0):
#                            brt = 0
#                        self.LED_share.write(brt)
#                        print('{:3d}'.format(brt))
                        print('Data collection cancelled')
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_in))
            
            elif self._state == _S2_COLLECT:
                ## @brief   List storing position data from encoder
                self.my_list = []
                ## @brief   List storing time data from encoder timer
                self.time_list = []
                self.my_list.append(self.position.read())
                self.time_list.append(utime.ticks_diff(_current_time, self._g_time))
                print('{:},{:}'.format(self.time_list, self.my_list))
#                self.row = [(self.my_list),(self.time_list)]
#                self.my_tuple = tuple(self.row)
#                self.my_Q.put(self.my_tuple)
                    
                if self._ser.any():
                    char_in = self._ser.read(1).decode()
                    if(char_in == 's' or char_in == 'S'):
                        print('Data collection cancelled')
                        self.transition_to(_S1_WAIT)
                        
                if (utime.ticks_diff(_current_time, self._g_time) >= 3000000):
                    self._g_time = 0
                    self.transition_to(_S3_PRINT)

#             else:
#                    self.transition_to(S3_PRINT)
#                if (self.my_Q.num_in() > 0):
#                    print(self.my_Q.get())
                            
            elif self._state == _S3_PRINT:
                _n = 0
                if len(self.my_list) > 0:
                    print(len(self.my_list))
                    print('{:},{:}'.format(self.time_list[_n], self.my_list[_n]))
                    _n += 1
                    if _n == len(self.my_list):
                        self.transition_to(_S1_WAIT)
#                if (self.my_Q.num_in() > 0):
#                    print(self.my_Q.get())
                    
#                if self.ser.any():
#                    char_in = self.ser.read(1).decode()
#                    
#                    if(char_in == 's' or char_in == 'S'):
#                        self.transition_to(S1_WAIT)
#                        
#                if (utime.ticks_diff(current_time, self.g_time) >= 3000000):
#                    self.transition_to(S1_WAIT)
#                    self.g_time = 0
                        
#                self.my_list = self.my_list.append(self.my_Q.get())
#                self.my_list.append(self.position.read())
#                if (self.my_Q.num_in() > 0):
                    
                
#                if (self.my_Q.num_in() > 0):
#                    print(self.my_Q.get())
                
            else:
                raise ValueError('Invalid State')
            
            self._next_time = utime.ticks_add(self._next_time, self.period)
            self._runs += 1
    
    def transition_to(self, _new_state):
        ''' @brief      Transitions the FSM to a new state
            @param      new_state The state to transition to next.
        '''
#        if (self.dbg):
#            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self._state = _new_state