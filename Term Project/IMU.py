''' @file       IMU.py
    @brief      A driver for reading from the Inertial Measurement Unit.
    @details    A driver used to perform calibration and read angular position and velocity of the IMU.
    @author     Nishka Chawla
    @author     Ronan Shaffer
    @date       12/9/2021
'''

from pyb import I2C
import struct
import time

# CONFIGMODE  = 0
# ACCONLY     = 1
# MAGONLY     = 2
# GYROONLY    = 3
# ACCMAG      = 4
# ACCGYRO     = 5
# MAGGYRO     = 6
# AMG         = 7
# IMU         = 8
# COMPASS     = 9
# M4G         = 10
# NDOF_FMC_OFF= 11
# NDOF        = 12

class IMU:
    ''' @brief      A driver class containing methods to read from the IMU. 
        @details    Class containing set_opr_mode, get_calib_stat, get_calib_coeff, 
                    set_calib_coeff, get_angles, and get_omegas methods to calibrate 
                    the IMU and read orientation information.
    '''
     
    def __init__ (self, i2c):
        ''' @brief              Initializes and returns an IMU object.
            @details            Class containing methods to read from the IMU. 
            @param i2c          Defines an i2c object as a controller.
        '''
        
        ## @brief   An i2c object used to read from the IMU.
        self.i2c = i2c
#         I2C(1, I2C.MASTER)
#         self.i2c.init(I2C.MASTER, baudrate = 20000)
#         self.i2c.init(I2C.SLAVE, addr = 0x28)
    
    def set_opr_mode (self):
         ''' @brief             Sets IMU operating mode.
             @details           Method to set the IMU operating mode to NDOF.
         '''
         self.i2c.mem_write(0x0C, 0x28, 0x3D)

    def get_calib_stat (self):
         ''' @brief             Returns calibration status. 
             @details           Method to view calibration status during manual calibration.
             @return            Calibration status of the IMU.
         '''
         ## @brief   Reads and stors calibration status from IMU register.
         cal_bytes = self.i2c.mem_read(1, 0x28, 0x35)
         
         ## @brief   Combines MSB and LSB for each calibration status value.
         calib_stat = (cal_bytes[0] & 0b11,
                      (cal_bytes[0] & 0b11 << 2) >> 2,
                      (cal_bytes[0] & 0b11 << 4) >> 4,
                      (cal_bytes[0] & 0b11 << 6) >> 6)
         return calib_stat   # bits <7:6> 3: yes 0: no
         print("Values:", calib_stat)
         print('\n')
         
    def get_calib_coeff (self):
         '''  @brief             Returns current calibration coefficients.
              @details           Method to read calibration coefficients from IMU register.
              @return            Current calibration coefficients from IMU register.
         '''
         ## @brief   A bytearray object of 22 bytes. 
         self.cal_buf = bytearray(22)
         self.i2c.mem_read(self.cal_buf, 0x28, 0x55)
         return self.cal_buf
         
    def set_calib_coeff (self, coeff):
         '''  @brief             Sets calibration coefficients.
              @details           Method to write calibration coefficients to the IMU register.
         '''
         self.i2c.mem_write(coeff, 0x28, 0x55)
         
    def get_angles (self):
         '''  @brief             Returns euler angle values from the IMU. 
              @details           Method to read theta_x, theta_y, and theta_z angles from the IMU.
              @return            Theta_x, theta_y, and theta_z angles.
         '''
         ## @brief   A bytearray object of 6 bytes. 
         euler_buf = bytearray(6)
         self.i2c.mem_read(euler_buf, 0x28, 0x1A)
         
         ## @brief   Variable used to unpack bytes into three signed integers.
         eul_signed_ints = struct.unpack('hhh', euler_buf)
         
         ## @brief   Variable used to scale integers.
         eul_vals = tuple(eul_int/16 for eul_int in eul_signed_ints)
#         print('Euler Angles:', eul_vals)
         return eul_vals
     
    def get_omegas (self):
         '''  @brief             Returns angular velocities from the IMU. 
              @details           Method to read theta_xdot, theta_ydot, and theta_zdot velocities from the IMU.
              @return            Theta_xdot, theta_ydot, and theta_zdot velocities.
         '''
         ## @brief   A bytearray object of 6 bytes. 
         omega_buf = bytearray(6)
         self.i2c.mem_read(omega_buf, 0x28, 0x14)
         
         ## @brief   Variable used to unpack bytes into three signed integers.
         omega_signed_ints = struct.unpack('hhh', omega_buf)
         
         ## @brief   Variable used to scale integers.
         omega_vals = tuple(omega_int/16 for omega_int in omega_signed_ints)
#         print('Angular Velocities:', omega_vals)
         return omega_vals
     
#if __name__ == '__main__':
#    import pyb
#    i2c = pyb.I2C(1,I2C.MASTER)
#    imu_drv = IMU(i2c)
#    
#    while True:
#        try:
#            imu_drv.set_opr_mode()
##            imu_drv.get_angles()
#            imu_drv.get_omegas()
##            imu_drv.get_calib_stat()
#            time.sleep(.2)
#        except KeyboardInterrupt:
#            print('Program Terminating')
#            break
        