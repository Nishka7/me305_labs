''' @file                       task_TouchPanel_TP.py
    @brief                      Interfaces with touch panel.
    @details                    Defines Task_TouchPanel class and run method.
    @author                     Nishka Chawla
    @author                     Ronan Shaffer
    @date                       12/9/2021
'''

import touchpanel_TP
import os
from ulab import numpy
import utime

## @brief   Instantiation of variable referencing calibration coefficients file. 
filename = "RT_cal_coeffs.txt"

class Task_TouchPanel:
    ''' @brief                  Interface with resistive touch panel.
        @details                Communicates between task_user, task_motor, and 
                                touch panel driver to relay touch panel position readings.
    '''
    
    def __init__(self, cf, cdx, state_panel):
        ''' @brief                  Interfaces with touch panel driver.
            @details                Communicates between task_user, task_motor, and touch panel
                                    driver to relay touch panel position readings.
            @param cf               A boolean flag used to check for contact on the touch panel.
            @param cdx              An index variable used to calibrate the touch panel. 
            @param state_panel      State variables from touch panel readings.
        '''
        ## @brief       Instantiation of an empty 5x3 matrix for touch panel readings used in manual calibration. 
        self.X = numpy.zeros([5,3])
        
 #       self.x = 0
        
 #       self.y = 0
        
        ## @brief       A shares.Share object for a touch panel calibration index. 
        #  @details     An index variable used to calibrate the touch panel. 
        self._cdx = cdx
        
        ## @brief       A shares.Share object for the touch panel contact flag.
        #  @details     A boolean flag used to check for contact on the Touch Panel.
        self._cf = cf
        
        ## @brief       An index used to iterate through IMU calibration once.
        self._runs = 0
        
        ## @brief       A boolean flag used to perform auto calibration.
        self.bool = 0
        
        ## @brief       A list of set locations on the touch panel used for manual calibration.
        self.Y = numpy.array([[-80,40],[80,40],[80,-40],[-80,-40],[0,0]])
        
        ## @brief       A shares.Share object for the touch panel state variables.
        #  @details     Contains list of touch panel state variables in order of x, y, x_dot, y_dot.
        self.state_panel = state_panel
        
#        self.current_time = utime.ticks_us()
        
#        self.last_time = 0
        
#        self.next_time = 0
        
#        self.x_poslast = 0
        
#        self.y_poslast = 0
        
        ## @brief       Touch panel driver object calling the driver class Touch_Panel.
        self.panel_drv = touchpanel.Touch_Panel()
        
#        self.period = 5000
        
        
    def autocalibrate(self):
        ''' @brief      Performs automatic calibration of the resistive touch panel.                  
            @details    Reads calibration coefficients from RT_cal_coeffs.txt
                        uses them to adjust position readings from the touch panel.
        '''
        
        with open(filename, 'r') as f:
            # Read the first line of the file
            
            ## @brief   Variable which reads and stores content of RT_cal_coeffs.txt file.
            cal_data_string = f.readline()
            # Split the line into multiple strings
            # and then convert each one to a float
            
            ## @brief   Splits RT_cal_coeffs.txt string into 6 integers, and converts them to floats.
            cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
#            cal_values = [cal_value for cal_value in cal_data_string.strip().split(',')]

            ## @brief   Contains list of calibration coefficients as floats.
            self.betalist = cal_values
            print(self.betalist)
            self._runs += 1
                 
        
    def manualcalibrate(self):
        ''' @brief      Performs manual calibration of the resistive touch panel.             
            @details    Requires the user to manually calibrate the touch panel and 
                        creates an RT_cal_coeffs.txt file with calibrated coefficients.                 
        '''
#            touchpanel.Touch_Panel.xyz_scan()
#        self.position = self.panel_drv.get_position()
        self.bool = 1
        with open(filename, 'w') as f:

            # Perform manual calibration
            if self._cdx.read() == 1 and self._cf.read() == 1:
                
                self.reading = self.panel_drv.get_readings()
                self.X[0][0] = float(self.reading[0])
                self.X[0][1] = float(self.reading[1])
                self.X[0][2] = float(self.reading[2])
                
            if self._cdx.read() == 2 and self._cf.read() == 1:
                
                self.reading = self.panel_drv.get_readings()
                self.X[1][0] = float(self.reading[0])
                self.X[1][1] = float(self.reading[1])
                self.X[1][2] = float(self.reading[2])
                
            if self._cdx.read() == 3 and self._cf.read() == 1:
                
                self.reading = self.panel_drv.get_readings()
                self.X[2][0] = float(self.reading[0])
                self.X[2][1] = float(self.reading[1])
                self.X[2][2] = float(self.reading[2])
                
            if self._cdx.read() == 4 and self._cf.read() == 1:
                
                self.reading = self.panel_drv.get_readings()
                self.X[3][0] = float(self.reading[0])
                self.X[3][1] = float(self.reading[1])
                self.X[3][2] = float(self.reading[2])
                
            if self._cdx.read() == 5 and self._cf.read() == 1:
                
                self.reading = self.panel_drv.get_readings()
                self.X[4][0] = float(self.reading[0])
                self.X[4][1] = float(self.reading[1])
                self.X[4][2] = float(self.reading[2])
                print(self.X)
                
                ## @brief   Beta is an approximation of the 3x2 coefficient matrix used to scale and shift position values.
                #  @details Beta = [ K_xx K_yx
                #                    K_xy K_yy
                #                    x_o  y_o ]
                beta = numpy.dot(numpy.dot(numpy.linalg.inv(numpy.dot(self.X.T,self.X)),self.X.T),self.Y)
                print(beta)
                self.betalist = [beta[0][0],beta[1][0],beta[0][1],beta[1][1],beta[2][0],beta[2][1]]
                (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = self.betalist
                self._runs += 1
                
                # Then, write the calibration coefficients to the file
                # as a string. The example uses an f-string, but you can
                # use string.format() if you prefer
                f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
                    
    def run(self):
        ''' @brief      Iterates through cooperative tasks.                   
            @details    Performs IMU calibration and communicates between task_motor 
                        and IMU driver to read and write IMU state variables.          
        '''
#        _current_time = utime.ticks_us()
#        if (utime.ticks_diff(_current_time, self._next_time) >= 0):
        self.panel_drv.xyz_scan()
        self._cf.write(self.panel_drv.xyz_scan())
        
        if self._runs < 1:
            
            if filename in os.listdir() and self.bool == 0:
                self.autocalibrate()
            else:
                self.manualcalibrate()
                
        else:
            
            if self._cf.read() == 1:
                
                self.reading = self.panel_drv.get_readings()
#                print('x reading:', self.reading[0], 'y reading:', self.reading[1])
#                self.next_time = self.current_time

                ## @brief   Calibration x- position.
                x_pos = float(self.betalist[0])*float(self.reading[0]) + float(self.betalist[1])*float(self.reading[0]) + float(self.betalist[4])
#                x_velo = (x_pos - self.x_poslast) / utime.ticks_diff(self.next_time, self.last_time)
#                self.x_poslast = x_pos

                ## @brief   Calibration y- position.
                y_pos = float(self.betalist[2])*float(self.reading[0]) + float(self.betalist[3])*float(self.reading[1]) + float(self.betalist[5])
                print('x:', x_pos, 'y:', y_pos)
#                y_velo = (y_pos - self.y_poslast) / utime.ticks_diff(self.next_time, self.last_time)
#                self.last_time = self.next_time
#                self.y_poslast = y_pos
                self.state_panel[0].write(x_pos)
#                self.state_panel[1].write(x_velo)
                self.state_panel[2].write(y_pos)
#                self.state_panel[3].write(y_velo)
                
#        self._next_time = utime.ticks_add(self._next_time, self.period)
            