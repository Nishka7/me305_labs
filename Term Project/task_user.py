''' @file                       task_user_TP.py
    @brief                      Interfaces with program user.
    @details                    Implements a finite state machine to take user input
                                commands and communicate with task_motor via shares.
    @author                     Nishka Chawla
    @author                     Ronan Shaffer
    @date                       12/9/2021
'''

import utime, pyb
from array import array
import shares

_S0_INIT            = 0
_S1_WAIT            = 1
_S2_COLLECT         = 2
_S3_GAIN_WAIT       = 3
_S4_GAIN_K11        = 4
_S5_GAIN_K12        = 5
_S6_GAIN_K13        = 6
_S7_GAIN_K14        = 7
_S8_GAIN_K21        = 8
_S9_GAIN_K22        = 9
_S10_GAIN_K23       = 10
_S11_GAIN_K24       = 11
_S12_IMU_CALIB      = 12
_S23_CALIBRATION1   = 23
_S24_CALIBRATION2   = 24
_S25_CALIBRATION3   = 25
_S26_CALIBRATION4   = 26
_S27_CALIBRATION5   = 27
_S28_CALIB_WAIT     = 28
_S29_BALANCE        = 29
_S16_ENABLE         = 16

class Task_User():
    ''' @brief      User interface task for cooperative multitasking.
        @details    Implements a finite state machine to take user input
                    commands and communicate with task_motor via shares.
    '''
    
    def __init__(self, period, gains1, gains2, motors, cf, cdx, balance_flag, enable_flag, imu_c_flag, state_IMU, state_panel):
        ''' @brief                  Communicates with other tasks.
            @details                Accepts user input and communicates with 
                                    task_IMU, task_motor, and task_TouchPanel.
            @param period           The period, in microseconds, between runs 
                                    of the task.
            @param gains1           The proportional gain values for controller 1.
            @param gains2           The proportional gain values for controller 2.                        
            @param motors           Object containing an index of motor objects.
            @param cf               A boolean flag used to check for contact on the Touch Panel.
            @param cdx              An index variable used to calibrate the touch panel. 
            @param balance_flag     A boolean flag used to enable the closed loop torque control of the platform. 
            @param enable_flag      A boolean flag used to enable and disable the motors.
            @param imu_c_flag       A boolean flag used to enable IMU calibration.
            @param state_IMU        State variables from IMU readings.
            @param state_panel      State variables from touch panel readings.
        '''
        ## @brief   Index used to iterate through data arrays.
        self._n = 0
        
        ## @brief   Instantiation of USB_VCP object.
        self._ser = pyb.USB_VCP()
        
        ## @brief   The period (in us) between task iterations.
        #  @details Time period that must elapse since last iteration before
        #           task runs again.
        self.period = period
        
        ## @brief   A shares.Share object for the balance flag.
        #  @details A boolean flag used to enable the closed loop torque control of the platform. 
        self.balance_flag = balance_flag
        
        ## @brief   A shares.Share object for the enable flag.
        #  @details A boolean flag used to enable and disable the motors.
        self.enable_flag = enable_flag
        
        ## @brief   A shares.Share object for the IMU calibration flag.
        #  @details A boolean flag used to enable IMU calibration.
        self.imu_c_flag = imu_c_flag
        
        ## @brief   A shares.Share object for the IMU state variables.
        #  @details Contains list of IMU state variables in order of theta_x, theta_y, thetadot_x, thetadot_y.
        self.state_IMU = state_IMU
        
        ## @brief   A shares.Share object for the touch panel state variables.
        #  @details Contains list of touch panel state variables in order of x, y, x_dot, y_dot.
        self.state_panel = state_panel
        
        ## @brief   Stores the proportional gains for controller 1.
        self.gains1 = gains1
        
        ## @brief   Stores the proportional gains for controller 2.
        self.gains2 = gains2
        
        ## @brief   Variable to set array size.
        self.array_size = int((10000000 /self.period.read()) + 2)
        
        ## @brief   List storing time data.
        self.time_list = array("f", [0] * self.array_size)
        
        ## @brief   List storing position data from encoder.
        self.theta_x_list = array("f", [0] * self.array_size)
        
        ## @brief   List storing delta data from encoder timer.
        self.theta_y_list = array("f", [0] * self.array_size)

        ## @brief   List storing delta data from encoder timer.
        self.theta_xdot_list = array("f", [0] * self.array_size)
        
        ## @brief   List storing the velocity setpoint.
        self.theta_ydot_list = array("f", [0] * self.array_size)

        ## @brief   The state to run on the next iteration of the finite state machine.
        self._state = _S0_INIT
        
        ## @brief   The number of runs of the state machine.
        self._runs = 0
        
        ## @brief   The utime.ticks_us() value associated with the next run of the FSM.
        self._next_time = utime.ticks_add(utime.ticks_us(), self.period.read())
        
        ## @brief   A shares.Share object for a touch panel calibration index. 
        #  @details An index variable used to calibrate the touch panel. 
        self._cdx = cdx
        
        ## @brief   A shares.Share object for the touch panel contact flag.
        #  @details A boolean flag used to check for contact on the Touch Panel.
        self._cf = cf
        
    def run(self):
        ''' @brief      Runs one iteration of the FSM
            @details    Accepts user input and communicates with task_IMU, 
                        task_motor, and task_TouchPanel.
        '''
        _current_time = utime.ticks_us()
        if (utime.ticks_diff(_current_time, self._next_time) >= 0):
            if self._state == _S0_INIT:
                print('Welcome!')
                print('Press \'k\' to set gains.')
                print('Press \'e\' to enable the motors.')
                print('Press \'g\' to collect data for 10 seconds')
                print('Press \'s\' to end data collection prematurely.')
                print('Press \'b\' to balance the platform')
                print('Press \'t\' to calibrate the touch panel.')
                print('Press \'i\' to calibrate the IMU.')
                self.transition_to(_S1_WAIT)
                
            elif self._state == _S1_WAIT:
                
                if self._ser.any():
                    ## @brief   Stores user input command as a string
                    char_in = self._ser.read(1).decode()
                        
                    if(char_in == 'b' or char_in == 'B'):
                        self.transition_to(_S29_BALANCE)
                        
                    elif(char_in == 'i'):
                        self.transition_to(_S12_IMU_CALIB)
                        
                    elif(char_in == 'e'):
                        self.transition_to(_S16_ENABLE)
                        
                    elif(char_in == 'g'):
                        print('Collecting Motor 1 data')
                        self._g_time = _current_time
                        self._idx = 0
                        self.transition_to(_S2_COLLECT)

                    elif(char_in == 's' or char_in == 'S'):
                        print('Data collection cancelled')
                        
                    elif(char_in == 'k'):
                        print('Chosen Motor 1 gains = [', self.gains1[0].read(), self.gains1[1].read(), self.gains1[2].read(), self.gains1[3].read(), ']')
                        print('Chosen Motor 2 gains = [', self.gains2[0].read(), self.gains2[1].read(), self.gains2[2].read(), self.gains2[3].read(), ']')
                        print('Press 1 to set the X Linear Position gain.')
                        print('Press 2 to set the Y Angular Position gain.')
                        print('Press 3 to set the X Linear Velocity gain.')
                        print('Press 4 to set the Y Angular Velocity gain.')
                        print('Press 5 to set the Y Linear Position gain.')
                        print('Press 6 to set the X Angular Position gain.')
                        print('Press 7 to set the Y Linear Velocity gain.')
                        print('Press 8 to set the X Angular Velocity gain.')
                        print('Press 9 to finish setting gain values.')
                        self.transition_to(_S3_GAIN_WAIT)
                        self._inputs = ''
                        
                    elif(char_in == 't'):
                        self._cdx.write(0)
                        self.transition_to(_S23_CALIBRATION1)
                        print('Touch top left grid point of panel.')

                    else:
                        print('Command \'{:}\' is invalid.'.format(char_in))
                        
            elif self._state == _S12_IMU_CALIB:
                
                self.imu_c_flag.write(1)
                self.transition_to(_S1_WAIT)
            
            elif self._state == _S29_BALANCE:
                
                if (self.balance_flag.read() == 0):
                    self.balance_flag.write(1)
                    self.transition_to(_S1_WAIT)
                    print('Balancing engaged...')
                
                elif (self.balance_flag.read() == 1):
                    self.balance_flag.write(0)
                    self.transition_to(_S1_WAIT)
                    print('Balancing disengaged.')
                
            elif self._state == _S16_ENABLE:
                
                if (self.enable_flag.read() == 0):
                    self.enable_flag.write(1)
                    self.transition_to(_S1_WAIT)
                    print('Motors enabled')
                
                elif (self.enable_flag.read() == 1):
                    self.enable_flag.write(0)
                    self.transition_to(_S1_WAIT)
                    print('Motors disabled')
                    
            elif self._state == _S3_GAIN_WAIT:
                
                if self._ser.any():
                    ## @brief   Stores user input command as a string
                    char_in = self._ser.read(1).decode()
                    
                    if(char_in == '1'):
                        self.transition_to(_S4_GAIN_K11)
                        print('Enter the X Linear Position gain.')
                        
                    if(char_in == '2'):
                        self.transition_to(_S5_GAIN_K12)
                        print('Enter the Y Angular Position gain.')
                        
                    if(char_in == '3'):
                        self.transition_to(_S6_GAIN_K13)
                        print('Enter the X Linear Velocity gain.')
                        
                    if(char_in == '4'):
                        self.transition_to(_S7_GAIN_K14)
                        print('Enter the Y Angular Velocity gain.')
                        
                    if(char_in == '5'):
                        self.transition_to(_S8_GAIN_K21)
                        print('Enter the Y Linear Position gain.')
                        
                    if(char_in == '6'):
                        self.transition_to(_S9_GAIN_K22)
                        print('Enter the X Angular Position gain.')
                        
                    if(char_in == '7'):
                        self.transition_to(_S10_GAIN_K23)
                        print('Enter the Y Linear Velocity gain.')
                        
                    if(char_in == '8'):
                        self.transition_to(_S11_GAIN_K24)
                        print('Enter the X Angular Velocity gain.')
                        
                    if(char_in == '9'):
                        print('Chosen Motor 1 gains = [', self.gains1[0].read(), self.gains1[1].read(), self.gains1[2].read(), self.gains1[3].read(), ']')
                        print('Chosen Motor 2 gains = [', self.gains2[0].read(), self.gains2[1].read(), self.gains2[2].read(), self.gains2[3].read(), ']')
                        self.transition_to(_S0_INIT)
                
            elif self._state == _S4_GAIN_K11:
                
                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S3_GAIN_WAIT)
                        
                        else:
                            self.gains1[0].write(float(self._inputs))
                            self._ser.write('\r')
                            print('Chosen gain for X Position is', self.gains1[0].read(), 'N')
                            print('Chosen Motor 1 gains = [', self.gains1[0].read(), self.gains1[1].read(), self.gains1[2].read(), self.gains1[3].read(), ']')
                            print('Chosen Motor 2 gains = [', self.gains2[0].read(), self.gains2[1].read(), self.gains2[2].read(), self.gains2[3].read(), ']')
                            self._inputs = ''
                            self.transition_to(_S3_GAIN_WAIT)
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                            
            elif self._state == _S5_GAIN_K12:
                
                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S3_GAIN_WAIT)
                        
                        else:
                            self.gains1[1].write(float(self._inputs))
                            self._ser.write('\r')
                            print('Chosen gain for Y Angular Position is', self.gains1[1].read(), 'N-m')
                            print('Chosen Motor 1 gains = [', self.gains1[0].read(), self.gains1[1].read(), self.gains1[2].read(), self.gains1[3].read(), ']')
                            print('Chosen Motor 2 gains = [', self.gains2[0].read(), self.gains2[1].read(), self.gains2[2].read(), self.gains2[3].read(), ']')
                            self._inputs = ''
                            self.transition_to(_S3_GAIN_WAIT)
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                            
            elif self._state == _S6_GAIN_K13:
                
                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S3_GAIN_WAIT)
                        
                        else:
                            self.gains1[2].write(float(self._inputs))
                            self._ser.write('\r')
                            print('Chosen gain for X Linear Velocity is', self.gains1[2].read(), 'N-s')
                            print('Chosen Motor 1 gains = [', self.gains1[0].read(), self.gains1[1].read(), self.gains1[2].read(), self.gains1[3].read(), ']')
                            print('Chosen Motor 2 gains = [', self.gains2[0].read(), self.gains2[1].read(), self.gains2[2].read(), self.gains2[3].read(), ']')
                            self._inputs = ''
                            self.transition_to(_S3_GAIN_WAIT)
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                            
            elif self._state == _S7_GAIN_K14:
                
                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S3_GAIN_WAIT)
                        
                        else:
                            self.gains1[3].write(float(self._inputs))
                            self._ser.write('\r')
                            print('Chosen gain for Y Angular Velocity is', self.gains1[3].read(), 'N-m-s')
                            print('Chosen Motor 1 gains = [', self.gains1[0].read(), self.gains1[1].read(), self.gains1[2].read(), self.gains1[3].read(), ']')
                            print('Chosen Motor 2 gains = [', self.gains2[0].read(), self.gains2[1].read(), self.gains2[2].read(), self.gains2[3].read(), ']')
                            self._inputs = ''
                            self.transition_to(_S3_GAIN_WAIT)
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                            
            elif self._state == _S8_GAIN_K21:
                
                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S3_GAIN_WAIT)
                        
                        else:
                            self.gains2[0].write(float(self._inputs))
                            self._ser.write('\r')
                            print('Chosen gain for Y Linear Position is', self.gains2[0].read(), 'N')
                            print('Chosen Motor 1 gains = [', self.gains1[0].read(), self.gains1[1].read(), self.gains1[2].read(), self.gains1[3].read(), ']')
                            print('Chosen Motor 2 gains = [', self.gains2[0].read(), self.gains2[1].read(), self.gains2[2].read(), self.gains2[3].read(), ']')
                            self._inputs = ''
                            self.transition_to(_S3_GAIN_WAIT)
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                            
            elif self._state == _S9_GAIN_K22:
                
                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S3_GAIN_WAIT)
                        
                        else:
                            self.gains2[1].write(float(self._inputs))
                            self._ser.write('\r')
                            print('Chosen gain for X Angular Position is', self.gains2[1].read(), 'N-m')
                            print('Chosen Motor 1 gains = [', self.gains1[0].read(), self.gains1[1].read(), self.gains1[2].read(), self.gains1[3].read(), ']')
                            print('Chosen Motor 2 gains = [', self.gains2[0].read(), self.gains2[1].read(), self.gains2[2].read(), self.gains2[3].read(), ']')
                            self._inputs = ''
                            self.transition_to(_S3_GAIN_WAIT)
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                            
            elif self._state == _S10_GAIN_K23:
                
                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S3_GAIN_WAIT)
                        
                        else:
                            self.gains2[2].write(float(self._inputs))
                            self._ser.write('\r')
                            print('Chosen gain for Y Linear Velocity is', self.gains2[2].read(), 'N-s')
                            print('Chosen Motor 1 gains = [', self.gains1[0].read(), self.gains1[1].read(), self.gains1[2].read(), self.gains1[3].read(), ']')
                            print('Chosen Motor 2 gains = [', self.gains2[0].read(), self.gains2[1].read(), self.gains2[2].read(), self.gains2[3].read(), ']')
                            self._inputs = ''
                            self.transition_to(_S3_GAIN_WAIT)
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                            
            elif self._state == _S11_GAIN_K24:
                
                if self._ser.any():
                    
                    char_in = self._ser.read(1).decode()
                    
                    if char_in == '\r' or char_in == '\n':
                        
                        if len(self._inputs) == 0:
                            self.transition_to(_S3_GAIN_WAIT)
                        
                        else:
                            self.gains2[3].write(float(self._inputs))
                            self._ser.write('\r')
                            print('Chosen gain for X Angular Velocity is', self.gains2[3].read(), 'N-m-s')
                            print('Chosen Motor 1 gains = [', self.gains1[0].read(), self.gains1[1].read(), self.gains1[2].read(), self.gains1[3].read(), ']')
                            print('Chosen Motor 2 gains = [', self.gains2[0].read(), self.gains2[1].read(), self.gains2[2].read(), self.gains2[3].read(), ']')
                            self._inputs = ''
                            self.transition_to(_S3_GAIN_WAIT)
                        
                    elif char_in.isdigit():
                        
                        self._inputs += char_in
                        self._ser.write(char_in)
                        
                    elif char_in == '-':
                        
                        if len(self._inputs) > 0:
                            pass
                        elif len(self._inputs) == 0:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '.':
                        
                        if '.' in self._inputs:
                            pass
                        else:
                            self._inputs += char_in
                            self._ser.write(char_in)
                            
                    elif char_in == '\b' or char_in == '\x7F':
                        
                        if len(self._inputs) == 0:
                            pass
                        else:
                            self._inputs = self._inputs[:-1]
                            self._ser.write('\x7F')
                            
                
            elif self._state == _S2_COLLECT:
                
                self.theta_x_list[self._idx] = self.state_IMU[0].read()
                self.theta_y_list[self._idx] = self.state_IMU[1].read()
                self.theta_xdot_list[self._idx] = self.state_IMU[2].read()
                self.theta_ydot_list[self._idx] = self.state_IMU[3].read()
                self.time_list[self._idx] = utime.ticks_diff(_current_time, self._res_time) / 1000000
                
                if self._ser.any():
                        
                    char_in = self._ser.read(1).decode()
                    
                    if(char_in == 's' or char_in == 'S'):
                        
                        print('Data collection cancelled')
                        self._s = 1
                            
                
                if self._idx == int(self.array_size-1) or self._s == 1:
                    
                    self.enable_flag.write(0)
                    self._res_time = 0
                    print('[ Time [s], Theta_X [rad], Theta_Y [rad], Theta_X_dot [rad/s], Theta_Y_dot [rad/s] ]')
                    print('{:},{:},{:},{:},{:}'.format(self.time_list[self._n], self.theta_x_list[self._n], self.theta_y_list[self._n], self.theta_xdot_list[self._n], self.theta_ydot_list[self._n]))
                    self._n += 1
                    
                    if self._n == self._idx:
                        
                        print('Done printing.')
                        self._n = 0
                        self.transition_to(_S1_WAIT)
                    
                else:
                    
                    self._idx += 1
                    
            elif self._state == _S23_CALIBRATION1:
                
                if self._cf.read() == 1:
                    self.transition_to(_S28_CALIB_WAIT)
                    print('Press 1 to continue.')
                    self._cdx.write(1)
                
            elif self._state == _S24_CALIBRATION2:
                
                if self._cf.read() == 1:
                    self.transition_to(_S28_CALIB_WAIT)
                    print('Press 2 to continue.')    
                    self._cdx.write(2)
                    
            elif self._state == _S25_CALIBRATION3:
                
                if self._cf.read() == 1:
                    self.transition_to(_S28_CALIB_WAIT)
                    print('Press 3 to continue.')    
                    self._cdx.write(3)
                            
            elif self._state == _S26_CALIBRATION4:
                        
                if self._cf.read() == 1:
                    self.transition_to(_S28_CALIB_WAIT)
                    print('Press 4 to continue.')    
                    self._cdx.write(4)
                            
            elif self._state == _S27_CALIBRATION5:
                     
                if self._cf.read() == 1:
                    self.transition_to(_S1_WAIT)
                    print('Calibration completed.')    
                    self._cdx.write(5)
                    
                                
            elif self._state == _S28_CALIB_WAIT:
                
                if self._ser.any():
                    ## @brief   Stores user input command as a string
                    char_in = self._ser.read(1).decode()
                    
                    if(char_in == '1'):
                        self.transition_to(_S24_CALIBRATION2)
                        print('Touch top right grid point of panel.')
                        
                    if(char_in == '2'):
                        self.transition_to(_S25_CALIBRATION3)
                        print('Touch bottom right grid point of panel.')
                        
                    if(char_in == '3'):
                        self.transition_to(_S26_CALIBRATION4)
                        print('Touch bottom left grid point of panel.')
                        
                    if(char_in == '4'):
                        self.transition_to(_S27_CALIBRATION5)
                        print('Touch center grid point of panel.')

            else:
                raise ValueError('Invalid State')
            
            self._next_time = utime.ticks_add(self._next_time, self.period.read())
            self._runs += 1
    
    def transition_to(self, _new_state):
        ''' @brief      Transitions the FSM to a new state
            @param      new_state The state to transition to next.
        '''
        self._state = _new_state