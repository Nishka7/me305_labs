''' @file                       task_motor_3.py
    @brief                      Interfaces with DRV8847 driver.
    @details                    Defines Task_Motor class and run method.
    @author                     Nishka Chawla
    @author                     Ronan Shaffer
    @date                       10/19/2021
'''

import pyb
import utime
import shares
import DRV8847

_S0_INIT = 0

_S1_RUN  = 1

_S2_STOP = 2

class Task_Motor:
    ''' @brief                  Interface with quadrature encoders
        @details                Communicates between task_user and encoder driver
                                to implement signals in hardware.
    '''
    
    def __init__(self, period, z_flags, enable_flag, fault_user_flag, positions, deltas, duties, motors, motor_drv):
        ''' @brief                  Interfaces with encoder driver.
            @details                Communicates between task_user and encoder 
                                    driver to relay encoder information.
            @param period           The period, in microseconds, between runs 
                                    of the task.
            @param z_flags          Boolean flags used to instruct the encoder 
                                    task to set the most recent encoder position 
                                    to zero.
            @param enable_flag      A boolean flag used to instruct the motor
                                    task to enable the motors from user input.
            @param fault_user_flag  A boolean flag used to inform the user of 
                                    a fault detection. Flag turned down when 
                                    cleared by the user.
            @param positions        The most recent orientation read by the 
                                    encoders.
            @param deltas           The number of ticks between the two most 
                                    recent positions recorded by the encoders.
            @param duties           A signed number holding the duty cycle of 
                                    the PWM signal sent to the motor.
            @param motors           Object containing an index of motor objects.
            @param motor_drv        A motor driver object in the DRV8847 class. 
        '''
        ## @brief   A shares.Share object for the fault user flag.
        self.fault_user_flag = fault_user_flag
        
        ## @brief   A shares.Share object for the enable flag.
        self.enable_flag = enable_flag
        
        ## @brief   A shares.Share object for the motor duty cycle.
        self.duties = duties
        
        ## @brief   A shares.Share object for motor objects.
        self.motors = motors
        
        ## @brief   A motor driver object in the DRV8847 class.
        self.motor_drv = motor_drv
        
        # The state to run on the next iteration of the finite state machine
        self._state = _S2_STOP
        
    def run(self):
        ''' @brief              Iterates through cooperative tasks.
            @details            Communicates between task_user and encoder 
                                driver to read and write to encoder object and 
                                call Encoder methods.
        '''

        if self._state == _S1_RUN:
            
            self.motor_drv.enable()
                
            self.motors[0].set_duty(self.duties[0].read())
            self.motors[1].set_duty(self.duties[1].read())
                
            if self.enable_flag.read() == 0:
                
                self.transition_to(_S2_STOP)
                
            if self.motor_drv.fault_cb_flag == 1:
                self.fault_user_flag.write(1)
                self.enable_flag.write(0)
                self.motor_drv.fault_cb_flag = 0
                self.transition_to(_S2_STOP)
            
        elif self._state == _S2_STOP:
            
            self.motor_drv.disable()
            
            if self.fault_user_flag.read() == 0 and self.enable_flag.read() == 1:
                
                self.motor_drv.fault_cb_flag = 0
                self.transition_to(_S1_RUN)
                
            
    def transition_to(self, _new_state):
        ''' @brief      Transitions the FSM to a new state
            @param      new_state The state to transition to next.
        '''
        self._state = _new_state