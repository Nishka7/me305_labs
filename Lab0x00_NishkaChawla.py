# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 15:19:31 2021

@author: Nishka Chawla
"""


def fib(idx): # fibonacci function uses a mathematical formula to calculate fibonacci numbers
    store = [0,1] # fib numbers are stored in a matrix to improve calculation time
    if idx == 1:
       return 1
    elif idx == 0:
       return 0
    elif idx > 1:
       X = len(store) - 1
       while X < idx:
           store.append(store[X] + store[X-1]) #fib numbers appended to matrix in line 11 
           X = X + 1
           
    return store[idx]


if __name__ == '__main__':

    while(True): 
        my_str = input('choose an index: ') #asks user input for fib index
    
        try:
            idx = int(my_str) #tries to convert given input into an integer
            if idx < 0:
                print('The index must be positive')  #error if index is negative
                continue
            
        except Exception:
            print('The index must be an integer') #error if index contains decimals
            
        else:
            print ('Fibonacci number at '
               'index {:} is {:}.'. format(idx,fib(idx))) #prints calculated fib number at given index
            
            stop = input('Enter q to quit or press enter to continue.') # asks for user input to quit program or continue
            if stop == 'q':
                break
            elif stop == '':
                continue
            
