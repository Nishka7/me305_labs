''' @file                       task_encoder.py
    @brief                      Interfaces with encoder driver.
    @details                    Defines Task_Encoder class and run method.
    @author                     Nishka Chawla
    @author                     Ronan Shaffer
    @date                       10/19/2021
'''

import pyb
import utime
import math
import encoder


_S1_UPDATE = 1

_serport = pyb.USB_VCP()

class Task_Encoder:
    ''' @brief                  Interface with quadrature encoders
        @details                Communicates between task_user and encoder driver
                                to implement signals in hardware.
    '''
    
    def __init__(self, period, z_flags, positions, deltas, encoders):
        ''' @brief              Interfaces with encoder driver.
            @details            Communicates between task_user and encoder 
                                driver to relay encoder information.
            @param period       The period, in microseconds, between runs of 
                                the task.
            @param z_flag       A boolean flag used to instruct the encoder
                                task to set the most recent encoder position
                                to zero.
            @param position     The most recent orientation read by the encoder.
            @param delta        The number of ticks between the two most recent 
                                positions recorded by the encoder.
            @param my_Q         A shares.Queue object used to store and print 
                                encoder position and delta values.
            @param encoder1     An encoder object in the task_encoder class.
        '''
        self._state = _S1_UPDATE
        self._runs = 0
        self.period = 10000
        self._next_time = utime.ticks_us() + self.period
        self.z_flags = z_flags
        self.positions = positions
        self.deltas = deltas
        self.encoders = encoders
        
        
        
    def run(self):
        ''' @brief              Iterates through cooperative tasks.
            @details            Communicates between task_user and encoder 
                                driver to read and write to encoder object and 
                                call Encoder methods.
        '''
        _current_time = utime.ticks_us()
        if (_current_time >= self._next_time):
            
            if (self._state == _S1_UPDATE):

                self.encoders[0].update()
                self.positions[0].write((2*math.pi/4000)*self.encoders[0].get_position())
                self.deltas[0].write(((2*math.pi/4000)*self.encoders[0].get_delta())/(int(self.period)/1000000))
                
                self.encoders[1].update()
                self.positions[1].write((2*math.pi/4000)*self.encoders[1].get_position())
                self.deltas[1].write(((2*math.pi/4000)*self.encoders[1].get_delta())/(int(self.period)/1000000))

                
                if (self.z_flags[0].read() == 1):
            
                    self.encoders[0].set_position(0)
                    self.z_flags[0].write(0)
                    
                if (self.z_flags[1].read() == 1):
                    
                    self.encoders[1].set_position(0)
                    self.z_flags[1].write(0)

            self._next_time += self.period
        
        self._runs += 1
        

       