'''@file                controllertesting.py
   @brief               Controller testing results.
   @details             A tuning of proportional controller gains shown 
                        graphically, and diagrams showing structure of  
                        closed-loop controller and tasks.

   @controllertesting

   @section sec_plots   Plots
                        \image html 0.5.png "Reference and measured velocities versus time for a Kp of 0.5." width = 700cm
                        
                        We started with a proportional gain of 0.5 %/rad/s, which was too low for the measured velocity of approximately 80 rad/s to reach the velocity setpoint of 150 rad/s.
                        
                        \image html 1.0.png "Reference and measured velocities versus time for a Kp of 1.0." width = 700cm
                        
                        A proportional gain of 1.0 %/rad/s increased the motor speed to 100 rad/s, but not by enough to reach the velocity setpoint.
                        
                        \image html 1.5.png "Reference and measured velocities versus time for a Kp of 1.5." width = 700cm
                        
                        A proportional gain of 1.5 %/rad/s further increased the motor speed to 110 rad/s, but still not by enough to reach the velocity setpoint. 
                        
                        \image html 2.0.png "Reference and measured velocities versus time for a Kp of 2.0." width = 700cm
                        
                        A proportional gain of 2.0 %/rad/s increased the motor speed to allow the motor to reach a steady state approximately equal to the velocity setpoint.
                        
                        \image html 2.5.png "Reference and measured velocities versus time for a Kp of 2.5." width = 700cm
                        
                        A proportional gain of 2.5 %/rad/s more quickly achieves the velocity setpoint and less frequently strays from the setpoint.
                
                        \image html 3.0.png "Reference and measured velocities versus time for a Kp of 3.0." width = 700cm
                        
                        A proportional gain of 3.0 %/rad/s was found to exceed the velocity setpoint, which is undesirable.

   @section sec_blk     Controller Block Diagram
                        \image html block_diagram.png "Controller Block Diagram" width = 700cm
                        
                        A block diagram depicting the structure of our closed-loop controller.

   @section sec_tasks   Task Diagrams and Finite State Machines
                        \image html task_diagram.png "Task Diagram" width = 700cm
                        
                        A task diagram containing shared information between all tasks running for the overall design of Lab 4 and their frequencies.
                        
                        \image html task_motor.png "Task_Motor State-transition Diagram" width = 700cm
                        
                        A finite state machine for the motor task. Please see task_motor_4.py for details.
                        
                        \image html task_user.png "Task_User State-transition Diagram" width = 700cm
                        
                        A finite state machine for the user task. Please see task_user_4.py for details.
                        
                        *Note: Our task_encoder only contains one state. 

   @date                November 15, 2021
'''