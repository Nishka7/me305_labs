''' @file DRV8847.py
 '''

import pyb
import utime


class DRV8847:
    ''' @brief    A motor driver class for the DRV8847 from TI.
         @details  Objects of this class can be used to configure the DRV8847
                    motor driver and to create one or moreobjects of the
                    Motor class which can be used to perform motor control.
    
     Refer to the DRV8847 datasheet here:
     https://www.ti.com/lit/ds/symlink/drv8847.pdf
     '''
     
    def __init__ (self, nSLEEP, nFAULT, tim):
         ''' @brief Initializes and returns a DRV8847 object.
         '''
#         self.IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
#         self.IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
#         
#         self.IN3 = pyb.Pin(pyb.Pin.cpu.B0, pyb.Pin.OUT_PP)
#         self.IN4 = pyb.Pin(pyb.Pin.cpu.B1, pyb.Pin.OUT_PP)
         
         self.nSLEEP = pyb.Pin(nSLEEP, pyb.Pin.OUT_PP)
         self.nFAULT = pyb.Pin(nFAULT, pyb.Pin.IN)
         self.tim = pyb.Timer(tim, freq = 20000)
         self.faultInt = pyb.ExtInt(self.nFAULT, mode=pyb.ExtInt.IRQ_FALLING, 
                                   pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
         self.fault_cb_flag = 0
         pass
    
    def enable (self):
         ''' @brief Brings the DRV8847 out of sleep mode.
         '''
         self.faultInt.disable()    # Disable fault interrupt
         self.nSLEEP.high()         # Re-enable the motor driver
         utime.sleep_us(25)         # Wait for the fault pin to return high
         self.faultInt.enable()     # Re-enable the fault interrupt
    
    def disable (self):
         ''' @brief Puts the DRV8847 in sleep mode.
         '''
         self.nSLEEP.low()
    
    def fault_cb (self, IRQ_src):
         ''' @brief Callback function to run on fault condition.
             @param IRQ_src The source of the interrupt request.
         '''
#         print('Fault detected')
         self.fault_cb_flag = 1
         self.disable()
    
    def motor (self, pinA, pinB, channel_A, channel_B):
         ''' @brief Initializes and returns a motor object associated with the DRV8847.
             @return An object of class Motor
         '''
         
         return Motor(pinA, pinB, channel_A, channel_B, self.tim)
         
             
class Motor:
    ''' @brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
    '''
    
    def __init__ (self, pinA, pinB, channel_A, channel_B, tim):
        ''' @brief Initializes and returns a motor object associated with the DRV8847.
            @details Objects of this class should not be instantiated directly. 
            Instead create a DRV8847 object and use that to create Motor objects using the method DRV8847.motor().
        '''
        
        self.tim = tim
        self.channel_A = self.tim.channel(channel_A, pyb.Timer.PWM, pin=pinA)
        self.channel_B = self.tim.channel(channel_B, pyb.Timer.PWM, pin=pinB)
    
    def set_duty(self, duty):
        ''' @brief Set the PWM duty cycle for the motor channel.
            @details This method sets the duty cycle to be sent to the motor to the given level. Positive values
            cause effort in one direction, negative values in the opposite direction.
            @param duty A signed number holding the duty cycle of the PWM signal sent to the motor
        '''
#        self.channel_1 = ch1
#        self.channel_2 = ch2
        self.duty = duty

        if self.duty > 0:
            
            self.channel_A.pulse_width_percent(self.duty)
            self.channel_B.pulse_width_percent(0)
            
        if self.duty < 0:
            
            self.channel_A.pulse_width_percent(0)
            self.channel_B.pulse_width_percent(-self.duty)