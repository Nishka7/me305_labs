''' @file                       encoder.py
    @brief                      A driver for reading from Quadrature Encoders
    @details
    @author                     Ronan Shaffer
    @author                     Nishka Chawla
    @date                       10/25/2021
'''

import pyb

class Encoder:
    ''' @brief                  Interface with quadrature encoders
        @details
    '''
    
    def __init__(self, pinA, pinB, tim_num):
        ''' @brief
            @details
        '''
        ## Assign Encoder 1 Pins on PCB to Pin B6 for PWM usage
        # self.pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
        # self.pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
        
        # self.pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
        # self.pinC7 = pyb.Pin(pyb.Pin.cpu.C7)
        
        self.ref_count = 0
        self.current_pos = 0
        
        # self.deltas = deltas
        
        ## Establish Timer for Encoder Counting
        self.period = 65535
        self.tim = pyb.Timer(tim_num, prescaler = 0, period = self.period)
        # self.tim8 = pyb.Timer(8, prescaler = 0, period = self.period)
        
        ## Link Encoder Channels
        self.tch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin=pinA)
        self.tch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin=pinB)
        
        
    def update(self):
        ''' @brief
            @details
            @return             The position of the encoder shaft
        '''        
        self.update_count = self.tim.counter()
        
        self.delta = self.update_count - self.ref_count
        if self.delta > 0 and self.delta > self.period/2:
            self.delta -= self.period
        if self.delta < 0 and abs(self.delta) > self.period/2:
            self.delta += self.period
            
        self.ref_count = self.tim.counter()
        
        self.current_pos += self.delta
        
        return self.current_pos
        
    def get_position(self):
        ''' @brief
            @details
        '''  
        
        return self.current_pos
    
    def set_position(self, position):
        ''' @brief
            @details
            @param  position    The new position of the encoder shaft
        '''
        self.current_pos = position

    def get_delta(self):
        ''' @brief
            @details
            @return             The change in position of the encoder shaft
                                between the two most recent updates
        '''
        return self.delta