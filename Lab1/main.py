# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 15:23:21 2021

@author: Ronan Shaffer
@author: Nishka Chawla
"""
import time
import pyb
import math
import utime
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

button_push = 0

#def SquareWaveFcn(IRQ_src):

def onButtonPressFCN(IRQ_src):
    global button_push
    button_push = 1
    
def update_sqw(current_time):
    return 100*(current_time%1.0<0.5)

def update_stw(current_time):
    return 100*(current_time%1.0)

def update_sin(current_time):
    return 100*(0.5 + 0.5*math.sin(math.pi*current_time/5))

if __name__ == '__main__':
    ## The state the Finite State Machine is about to run
    state = 0
    ## The number of iterations of the Finite State Machine
    runs = 0
    ## Associates button press with callback function
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    while True:
        try:                        # If no keyboard interrupt, run FSM
            
            if state == 0:
                # run state 0 code
                print('Welcome! Rules: Press the blue button to cycle through LED patterns')
                state = 1           # Transition to state 1
            
            elif state == 1:
                # run state 1 code: wait for button push to go to square wave
                if button_push == 1:
                    state = 2       # Transition to state 2
                    button_push = 0
                    print('Square Wave pattern selected.')
                    startTime2 = utime.ticks_ms()
            
            elif state == 2:
                # run state 2 code: square wave
                stopTime2 = utime.ticks_ms()
                current_time = utime.ticks_diff(stopTime2, startTime2)
                current_time = current_time/1000
                t2ch1.pulse_width_percent(update_sqw(current_time))
                if button_push == 1:
                    state = 3       # Transition to state 3
                    button_push = 0
                    print('Sine Wave pattern selected.')
                    startTime3 = utime.ticks_ms()
                            
            elif state == 3:
                # run state 3 code: sine wave
                stopTime3 = utime.ticks_ms()
                current_time = utime.ticks_diff(stopTime3, startTime3)
                current_time = current_time/1000
                t2ch1.pulse_width_percent(update_sin(current_time))
                if button_push == 1:
                    state = 4       # Transition to state 4
                    button_push = 0
                    print('Sawtooth pattern selected.')
                    startTime4 = utime.ticks_ms()
            
            elif state == 4:
                # run state 4 code: sawtooth wave
                stopTime4 = utime.ticks_ms()
                current_time = utime.ticks_diff(stopTime4, startTime4)
                current_time = current_time/1000
                t2ch1.pulse_width_percent(update_stw(current_time))
                if button_push == 1:
                    state = 2       # Transition to state 2
                    button_push = 0
                    print('Square Wave pattern selected.')
                    startTime2 = utime.ticks_ms()
                
            time.sleep(0.01)        # Slow down FSM so we can read output
            
            runs += 1               # Increment run counter to track number
                                    # of FSM runs
            
        except KeyboardInterrupt:   # If keyboard interrupt, exit loop
            break
        
    print('Program Terminated')