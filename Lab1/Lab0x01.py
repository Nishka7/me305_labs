#!/usr/bin/env python3
# -*- coding: utf-8 -*-
''' 
Created on Tue Sep 28 15:23:32 2021
@author: nishkachawla

@file       FSM_template.py
@brief      Finite State Machine Skeleton Code
@details    This file will be used to demonstrate one method 
            for implementing a finite state machine in Python
'''


#all imports at the top near docstring
import time

#all functions should be defined before the main part of the program

#the main program should go after function definitions and run continuously 
if __name__ == '__main__':
    ## The current state for this iteration of the FSM
    state = 0
    
    #the number of iterations performed by the FSM
    runs = 0
    
    
    while(True):
        try:                            #if no keyboard interrupt, run FSM
            if (state==0):
                #run state 0 code
                print('Running state 0')
                
                state = 1               #transition to state 1
            
            elif (state==1):
                #run state 1 code
                print('Running state 1')
                
                state = 2               #transition to state 2
            
            elif (state==2):
                #run state 2 code
                print('Running state 2')
                
                state = 3               #transition to state 3
            
            elif (state==3):
                #run state 3 code
                print('Running state 3')
                
                state = 4               #transition to state 4
            
            elif (state==4):
                #run state 4 code
                print('Running state 4')
                
                state = 2               #transition to state 2
                
            time.sleep(1)               #basic delay function (1) delays once per second/slow down FSM 
                                        #so we can actually see the console output
            runs += 1
            
                            
        except KeyboardInterrupt:       #if keyboard interrupt, exit loop
            break
        
        
    print('Program Terminating')
        
        
        
        